/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Router from './src/pages/Router'
import MainPage from './src/pages/MainPage'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router);
