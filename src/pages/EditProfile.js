import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, Image, StatusBa, TouchableOpacity, Modal, TextField } from 'react-native';
import { Button, Icon, InputItem, WhiteSpace } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios';

class EditProfile extends Component {

    state = {
        firstName: '',
        lastName: '',
    }

    onClickBack = () => {
        this.props.history.push('/profile', {
            item: 'profile'
        })
    }

    onSave = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/ticGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.token}`
                },
            }
            )
        })
            .then(response => {
                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/main', {
                    item: 'profile'
                })
            }).catch(e => {
                console.log("error " + e)
            })

    }

    render() {
        const { user, editUser } = this.props
        console.log(user)
        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="black" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Image style={{ width: 90, height: 90 }}
                            source={require('../img/logo/muraKim.png')} />
                    </View>
                    <View style={styles.back}>

                    </View>

                </View>
                <View style={styles.content}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 24, color: '#455a64' }}>Edit Information</Text>
                    </View>
                    <View style={{ marginTop: 15, marginBottom: 10 }}>
                        <View style={{ width: '90%', margin: 5 }}>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                value={this.state.firstName}
                                onChange={firstName => {
                                    this.setState({
                                        firstName,
                                    });
                                }}
                                placeholder="First Name"
                            >
                            </InputItem>
                        </View>
                        <View style={{ width: '90%' }}>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                value={this.state.lastName}
                                onChange={lastName => {
                                    this.setState({
                                        lastName,
                                    });
                                }}
                                placeholder="Last Name"
                            >
                            </InputItem>
                        </View>
                    </View>
                    <Button
                        style={styles.buttons}
                        onPress={() => this.onSave()}
                        color="white"
                        type="primary"
                    // onPress={() => this.setModalVisible(true)}
                    >
                        <Icon name="save" size="sm" color="white" /> Save
                    </Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',    
        // justifyContent: 'center',
        backgroundColor: '#e1e7ea',
    },
    content: {
        margin: 20,
        padding: 10,
        backgroundColor: 'white'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        flexDirection: 'row'
    },
    text: {

    },
    TextContent: {
        flexDirection: 'row',
    },
    buttons: {
        margin: 15,
        backgroundColor: '#ffcc00',
        borderColor: '#ffcc00',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)
