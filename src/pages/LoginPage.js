import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput,Image } from 'react-native';
import { Icon, InputItem, Button } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'

import axios from 'axios';



class LoginPage extends Component {

    state = {
        visible: false,
        email: 'test2@gmail.com',
        password: '123456',
        firstname: '',
        lastname: '',
    }

    navigateToMainPage = () => {
        this.props.history.push('/main')
    }

    navigateToRegisterPage = () => {
        this.props.history.push('/register')
    }

    navigateToProfile = () => {
        this.props.history.push('/profile')
    }

    onChangeValue = (index, value) => this.setState({ [index]: value })
    onLogin = () => {
        this.setState({ visible: true })
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                email: 'test2@gmail.com',
                password: '123456'
                // token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
                // eyJlbWFpbCI6ImtpbUBnbWFpbC5jb20iLCJpZCI6IjVjNzVh
                // MTg1OTU1NzJiMDAxMDBjYzczMiIsImV4cCI6MTU1NjM5Njkz
                // MywiaWF0IjoxNTUxMjEyOTMzfQ.dIGiuDeY-rYYS3OzzQYj9
                // f2XKnq9mAWQ8MdKJsc4gZs"
            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.addUser(data.user);
            this.props.history.push('/main', {
                item: 'movies'
            })
        }).catch(e => {
            console.log("error " + e)
            alert(e.response.data.errors.email)
        })
        // axios({
        //     url: 'https://zenon.onthewifi.com/ticGo/users',
        //     method: 'get',
        //     params: {

        //     }
        // })
    }

    render() {
        const { username, password } = this.state
        const { user, addUser } = this.props
        console.log("renderlogin",user)
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <View style={styles.header}>
                        <Text style={{ color: 'white' }}>LOGIN</Text>
                    </View>
                </View>
                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <Text>
                    {this.state.email} - {this.state.password}
                </Text>
                <View style={{ width: '90%' }}>
                <Text style={{ color: 'white'}}>Email</Text>
                    <InputItem
                        style={{ color: 'white' }}
                        clear
                        value={this.state.email}
                        onChange={email => {
                            this.setState({
                                email,
                            });
                        }}
                        placeholder="Email"
                    >
                        <Icon name="user" size="md" color="white" />
                    </InputItem>
                </View>
                <View style={{ width: '90%' }}>
                <Text style={{ color: 'white'}}>Password</Text>
                    <InputItem
                        style={{ color: 'white' }}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.password}
                        onChange={password => {
                            this.setState({
                                password,
                            });
                        }}
                        placeholder="Password"
                    >
                        <Icon name="lock" size="md" color="white" />
                    </InputItem>
                </View>

                <View style={styles.button}>
                    {/* <Button
                        title="Login"
                        color="#34444b"
                        onPress={() => this.onLogin()}
                    /> */}
                    <Button loading={this.state.visible}
                        onPress={() => this.onLogin()}
                        activeStyle={{ backgroundColor: '#a6a6a6' }}
                        style={styles.loginButton}
                        type="primary"
                    >
                        <Text style={{ color: '#39281B' }}>Login</Text>
                    </Button>
                    {/* <Button type="primary" onPress={() => { this.onLogin() }} >Login</Button> */}
                </View>
                
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>Don't have an account yet?</Text>
                    <TouchableOpacity onPress={() => { this.navigateToRegisterPage() }}><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        
        //#009999
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row'

    },
    signupText: {
        color: 'gray',
        fontSize: 16
    },
    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500'
    },
    button: {
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12
    },
    inputBox: {
        width: 300,
        backgroundColor: 'white',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: 'white',
        marginVertical: 10
    },
    loginButton: {
        backgroundColor: '#CDCD8F',
        borderRadius: 30,
        borderColor: '#CDCD8F'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
});

const mapStateToProps = ({ user }) => ({
    username: user.username,
    password: user.password,
})

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)(LoginPage)

