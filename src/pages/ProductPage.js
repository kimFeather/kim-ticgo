import React from 'react';
import { StyleSheet, Text, View, ScrollView, Modal, Image, TextInput, TouchableOpacity, Alert, FlatList } from 'react-native'
import { Carousel, Icon, Button, SearchBar, TabBar, Card } from '@ant-design/react-native';
import axios from 'axios'

class ProductPage extends React.Component {

    state = {
        products: [],
        dates: new Date()

    }

    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }


    onClickBack = () => {
        const { push } = this.props
        push('/main')
    }


    // UNSAFE_componentWillMount() {
    //     console.log(this.props.location)
    //     axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
    //         params: {
    //             date: 1550749200000
    //         }
    //     })
    //         .then(response => {
    //             return response.data
    //         })
    //         .then(data => {
    //             this.setState({ products: data })
    //             console.log(data)
    //         })
    //         .catch(err => { console.log(err) })
    //         .finally(() => { console.log('Finally') })
    // }

    UNSAFE_componentWillMount() {
        console.log(this.props.location)
        var date = this.state.dates.getTime();
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
            { params: { date: date } }
        )
            .then(response => {
                this.setState({
                    movie: response.data,
                })
                console.log('Movie: ', this.state.movie)
                console.log('State didmount: ', this.state.dates)
                console.log('Date Now: ', Date.now())
                console.log('var Date: ', date)
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }


    renderContent = (dates) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
            { params: { date: dates } }
        )
            .then(response => {
                this.setState({
                    movie: response.data,
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }



    navigateToMainPage = () => {
        this.props.history.replace('/main', this.state)
    }

    navigateToProfile = () => {
        this.props.history.push('/profile')
    }

    navigateToHistory = () => {
        this.props.history.push('/history')
    }

    navigateToSeatSelect = (item) => {
        const { push } = this.props.history
        const movie = this.props.location.state.item
        push('/seat', {
            item: item, movie: movie
        })
    }



    render() {

        console.log("product page", this.state.products)
        console.log("find item", this.state.item)

        const movie = this.props.location.state.item
        var tab1 = Date.now()
        var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
        tab2 = tab2.getTime()
        var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
        tab3 = tab3.getTime()


        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <TouchableOpacity style={styles.backButton} onPress={this.navigateToMainPage}>
                        <Text style={{ color: '#CDCD8F', fontSize: 14 }}><Icon name="left" size="md" color="#CDCD8F" />Back</Text>
                    </TouchableOpacity>
                    <View style={styles.header}>
                        <Text style={{ color: 'white' }}>MOVIE</Text>
                    </View>
                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />

                <ScrollView style={styles.scrollContainer}>
                    <View style={styles.dateContent}>
                        <TouchableOpacity style={[styles.dateButton]} onPress={() => { this.renderContent(tab1) }}>
                            <Text style={{ fontSize: 14, color: '#CDCD8F' }}>TODAY </Text>
                            <Text style={{ fontSize: 24, color: '#CDCD8F' }}>{new Date().toLocaleDateString("en-US", { day: 'numeric', })}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.dateButton} onPress={() => { this.renderContent(tab2) }}>
                            <Text style={{ fontSize: 14, color: '#CDCD8F' }}>{new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", { weekday: 'short' })}</Text>
                            <Text style={{ fontSize: 24, color: '#CDCD8F' }}>{new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", { day: 'numeric' })}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.dateButton]} onPress={() => { this.renderContent(tab3) }}>
                            <Text style={{ fontSize: 14, color: '#CDCD8F' }}>{new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", { weekday: 'short' })}</Text>
                            <Text style={{ fontSize: 24, color: '#CDCD8F' }}>{new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", { day: 'numeric' })}</Text>
                        </TouchableOpacity>
                    </View>

                    <Image style={styles.footing}
                        source={require('../img/object/footing.png')}
                    />

                    <View style={styles.tabs}>
                        <View>
                            <View style={styles.cinemaTitle}>
                                
                                <Text style={{ fontSize: 14, color: '#CDCD8F' }}> Cinema Screen</Text>
                            </View>
                            <FlatList
                                numColumns={3}
                                data={this.state.movie}
                                renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')

                                    <TouchableOpacity style={{ margin: 2, padding: 6, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.navigateToSeatSelect(item)} >
                                        <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                        <Button activeStyle={{ backgroundColor: '#80dfff' }} onPress={() => { this.navigateToSeatSelect(item) }}>
                                            <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                        </Button>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                        <Image style={styles.footing}
                            source={require('../img/object/footing.png')}
                        />

                        <View>
                            <View style={styles.cinemaTitle}>
                                <Image style={styles.cinemaLogo}
                                    source={require('../img/logo/imax.jpg')}
                                />

                                <Text style={{ fontSize: 14, color: '#CDCD8F' }}> IMAX 3D </Text>
                            </View>
                            <FlatList
                                numColumns={3}
                                data={this.state.movie}
                                renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')

                                    <TouchableOpacity style={{ margin: 2, padding: 6, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.navigateToSeatSelect(item)} >
                                        <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                        <Button activeStyle={{ backgroundColor: '#80dfff' }} onPress={() => { this.navigateToSeatSelect(item) }}>
                                            <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                        </Button>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                        <Image style={styles.footing}
                            source={require('../img/object/footing.png')}
                        />

                        <View>
                            <View style={styles.cinemaTitle}>
                                <Image style={styles.cinemaLogo}
                                    source={require('../img/logo/4dx.png')}
                                />

                                <Text style={{ fontSize: 14, color: '#CDCD8F' }}> 4DX</Text>
                            </View>
                            <FlatList
                                numColumns={3}
                                data={this.state.movie}
                                renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')

                                    <TouchableOpacity style={{ margin: 2, padding: 6, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.navigateToSeatSelect(item)} >
                                        <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                        <Button activeStyle={{ backgroundColor: '#80dfff' }} onPress={() => { this.navigateToSeatSelect(item) }}>
                                            <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                        </Button>
                                    </TouchableOpacity>
                                )}
                            />
                        </View>

                        <Image style={styles.footing}
                            source={require('../img/object/footing.png')}
                        />



                        {/* รอบหนัง1 */}
                        {/* <View style={[styles.boxContent]}>
                            <View style={[styles.rowHeader, styles.center]}>
                                <Text style={styles.textHead}>

                                    <Icon name="sound" size="md" color="#f47373" />
                                    <Text>
                                        {this.props.location.state.item.soundtracks[0]}
                                    </Text>

                                    <Text>
                                        {this.state.products.map(product => {
                                            return <Text> {product.cinema.name} ,  {product.startDateTime}  </Text>

                                        })}
                                    </Text>
                                </Text>
                            </View>
                        </View> */}



                        <View style={[styles.boxContent]}>
                            <View style={[styles.rowHeader]}>
                                <Image source={{ uri: this.props.location.state.item.image }}
                                    style={{ width: '100%', height: 300 }} />
                            </View>
                            <View style={[styles.rowHeader, styles.center]}>
                                <Text style={styles.textHead}>
                                    {this.props.location.state.item.name}
                                </Text>
                                <Text style={styles.textHead}>
                                    <Icon name="clock-circle" size="md" color="#f47373" />
                                    {this.props.location.state.item.duration} min
                        </Text>
                            </View>


                        </View>
                        <View style={{ flexDirection: 'row', }}>
                            <TouchableOpacity style={[styles.loginButton]} >
                                <Text style={{ fontSize: 14, color: 'black' }}>ADD TO WATCHLIST </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.loginButton]} >
                                <Text style={{ fontSize: 14, color: 'black' }}>Share</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>
                <View style={styles.footer}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#CDCD8F"
                        barTintColor="black"
                        opacity="0.6"
                    >
                        <TabBar.Item
                            title="Movie"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === 'blueTab'}
                            onPress={() => this.navigateToMainPage()}
                        >
                            {/* {this.renderContent('Life Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="star" />}
                            title="Mymovie"
                            selected={this.state.selectedTab === 'redTab'}
                            onPress={() => this.navigateToHistory()}
                        >
                            {/* {this.renderContent('Koubei Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="user" />}
                            title="Profile"
                            selected={this.state.selectedTab === 'greenTab'}
                            onPress={() => this.navigateToProfile()}
                        >
                            {/* {this.renderContent('Friend Tab')} */}
                        </TabBar.Item>

                    </TabBar>

                </View>
            </View >
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 50,
        backgroundColor: 'white',
        flexDirection: 'row',
        opacity: 60
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: '#8b9dc3',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    scrollContainer: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
        flexDirection: 'column',
    },
    boxContent: {
        flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        margin: 10
    },
    rowHeader: {
        flex: 1,
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'white'
    },
    slideContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
    },
    wrapper: {
        backgroundColor: '#fff',
    },
    cardWrapper: {
        flex: 1,
        padding: 5,
    },
    cardImage: {
        width: 186.7,
        height: 276.3,
    },
    cardTextWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 4,
        backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardText: {
        color: '#FFF',
        fontWeight: 'bold'
    },
    dateButton: {
        height: 50,
        width: 65,
        marginHorizontal: 20,
        borderRadius: 5,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    dateContent: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#33353a',
        padding: 2,
    },
    backButton: {
        padding: 5,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footing: {
        width: 'auto'
    },
    loginButton: {
        height: 35,
        width: 180,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: '#CDCD8F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cinemaTitle: {
        flex: 1,
        flexDirection: 'row',
    },
    cinemaLogo: {
        height: 30,
        width: 60,
        marginHorizontal: 10
    }




})

export default ProductPage