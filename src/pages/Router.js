import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native';
import EditProfile from './EditProfile';
import LoginPage from './LoginPage';
import HistoryPage from './HistoryPage';
import MainPage from './MainPage';
import PaymentPage from './PaymentPage';
import ProductPage from './ProductPage';
import ProfilePage from './ProfilePage';
import SeatSelectPage from './SeatSelectPage';
import TicketPage from './TicketPage';
import RegisterPage from './RegisterPage';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { store , history } from '../store/AppStore';
// import { history } from '../reducers/UserReducer';




class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/login" component={LoginPage} />
                        <Route exact path="/main" component={MainPage} />
                        <Route exact path="/profile" component={ProfilePage} />
                        <Route exact path="/editprofile" component={EditProfile} />
                        <Route exact path="/history" component={HistoryPage} />
                        <Route exact path="/payment" component={PaymentPage} />
                        <Route exact path="/seat" component={SeatSelectPage} />
                        <Route exact path="/product" component={ProductPage} />
                        <Route exact path="/ticket" component={TicketPage} />
                        <Route exact path="/register" component={RegisterPage} />

                        <Redirect to="/main" />
                    </Switch>
                </ConnectedRouter>
            </Provider>


            // <NativeRouter>
            //     <Switch>
            //         <Route exact path="/login" component={LoginPage}/>
            //         <Route exact path="/main" component={MainPage}/>
            //         <Route exact path="/profile" component={ProfilePage}/>
            //         <Route exact path="/editprofile" component={EditProfile}/>
            //         <Route exact path="/history" component={HistoryPage}/>
            //         <Route exact path="/payment" component={PaymentPage}/>
            //         <Route exact path="/seat" component={SeatSelectPage}/>
            //         <Route exact path="/product" component={ProductPage}/>
            //         <Route exact path="/ticket" component={TicketPage}/>

            //         <Redirect to="/main" />
            //     </Switch>
            // </NativeRouter>
        )
    }
}


export default Router