import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import { InputItem, WhiteSpace, Icon, Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';
export default class RegisterPage extends Component {

    state = {

        email: '',
        password: '',
        confirmPassword: '',
        firstname: '',
        lastname: '',
    }

    onRegister = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '' || this.state.firstname === '' || this.state.lastname === '') {
            Alert.alert('Please do not leave input fields blanked')
        } else {
            if (this.state.password !== this.state.confirmPassword) {
                await Alert.alert('Incorrect Input', 'Confirmed password is not matched.')
            } else {
                try {
                    await axios.post('https://zenon.onthewifi.com/ticGo/users/register?email', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstname,
                        lastName: this.state.lastname
                    })
                    await Alert.alert('Register successful')
                    await this.props.history.push('/login')
                } catch (error) {
                    console.log('signup res', error.response);
                }

            }

        }
    }

    signup = () => {
        // axios({
        //     url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
        //     method: 'post',
        //     data: {
        //         email: this.state.email,
        //         password: this.state.password,
        //         firstName: this.state.firstname,
        //         lastName: this.state.lastname
        //     }
        // }).then(res => {
        //     const { data } = res
        //     const { user } = data
        //     this.props.history.push('/login')
        // }).catch(e => {
        //     console.log("error ", e.response)
        // })
    }
    
    loginPage = () => {
        this.props.history.push('/login')
    }


    navigateToProfile = () => {
        this.props.history.replace('/profile', this.state)
    }

    render() {
        // const { user } = this.props
        // console.log(user)
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <TouchableOpacity style={styles.backButton} onPress={this.navigateToProfile}>
                        <Text style={{ color: '#CDCD8F', fontSize: 14 }}><Icon name="left" size="md" color="#CDCD8F" />Back</Text>
                    </TouchableOpacity>
                    <View style={styles.header}>
                        <Text style={{ color: 'white' }}>REGISTATION</Text>
                    </View>
                </View>
                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />

                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />
                <View style={styles.content}>
                    <Text>
                        EMAIL : {this.state.email}
                    </Text>
                    <Text>
                        PASSWORD :  {this.state.password}
                    </Text>
                    <Text>
                        NAME : {this.state.firstname}  {this.state.lastname}
                    </Text>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.email}
                        // onChange={email => {
                        //     this.setState({
                        //         email,
                        //     });
                        // }}
                        onChange={value => { this.setState({ email: value }) }}
                        placeholder="Email"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.password}
                        // onChange={password => {
                        //     this.setState({
                        //         password,
                        //     });
                        // }}
                        onChange={value => { this.setState({ password: value }) }}
                        placeholder="Password"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.confirmPassword}
                        // onChange={confirmPassword => {
                        //     this.setState({
                        //         confirmPassword,
                        //     });
                        // }}
                        onChange={value => { this.setState({ confirmPassword: value }) }}
                        placeholder="Confirm Password"
                    >
                    </InputItem>

                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.firstname}
                        // onChange={firstname => {
                        //     this.setState({
                        //         firstname,
                        //     });
                        // }}
                        onChange={value => { this.setState({ firstname: value }) }}
                        placeholder="First name"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.lastname}
                        // onChange={lastname => {
                        //     this.setState({
                        //         lastname,
                        //     });
                        // }}
                        onChange={value => { this.setState({ lastname: value }) }}
                        placeholder="Last name"
                    >
                    </InputItem>

                    <TouchableOpacity style={[styles.regisButton]} onPress={() => this.onRegister()}>
                        <Text style={{ fontSize: 14, color: '#CDCD8F' }}>REGIS </Text>
                    </TouchableOpacity>


                    <WhiteSpace />
                    <WhiteSpace />

                    <Text style={{ color: 'gray' }}>──────────────────────────────</Text>
                    <View style={styles.signupTextCont}>
                        <Text style={styles.signupText}>If you have an account!</Text>
                        <TouchableOpacity onPress={() => { this.loginPage() }}><Text style={styles.signupButton}> Signin</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },

    signupTextCont: {
        flex: 1,
        flexDirection: 'row'

    },

    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    },

    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500'
    },
    content: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        height: 50,
        width: 350,
        marginHorizontal: 20,
        borderRadius: 5,
        padding: 10,
        color: 'black',
        backgroundColor: '#CDCD8F',
    },
    textTitle: {
        color: '#ffffff',
        fontSize: 24
    },
    button: {
        flex: 1,
        alignItems: 'center',
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12,
        justifyContent: 'center'
    },
    backButton: {
        padding: 5,
        backgroundColor: 'black',
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    footing: {
        width: 'auto'
    },
    regisButton: {
        height: 50,
        width: 350,
        marginHorizontal: 20,
        borderRadius: 5,
        backgroundColor: '#39281B',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#39281B'
    },
});
