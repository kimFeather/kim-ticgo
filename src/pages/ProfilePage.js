import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Modal,
    Image,
    TextInput,
    TouchableOpacity, Alert, FlatList, Dimensions
} from 'react-native'
import { Button, Icon, InputItem, WhiteSpace, TabBar, Card } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'

class ProfilePage extends React.Component {

    state = {
        products: [],
        // isLogin: false,
        visible: false,
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        imagePath: ''

    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'profileTab',
        };
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    componentDidMount() {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    UNSAFE_componentWillMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => {
                return response.data
            })
            .then(data => {
                this.setState({ products: data })
                console.log(data)
            })
    }

    onPressImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }


    clickProduct = (product) => {
        console.log(product);
        const tempState = this.state
        tempState.productClick = product
        this.props.history.push('/product', { item: product })

    }

    navigateToProfile = () => {
        this.props.history.push('/profile')
    }

    navigateToHistory = () => {
        this.props.history.push('/history')
    }

    navigateToLogin = () => {
        this.props.history.push('/login')
        this.setState({ isLogin: true })
        console.log("islogin",this.state.isLogin)
        // logingin = true
    }

    navigateToRegis = () => {
        this.props.history.push('/register')
    }

    navigateToMain = () => {
        this.props.history.push('/main')
    }
    navigateToItemMoviePage = () => {
        const { push } = this.props
        push('/edituser')
    }

    navigateToEditUserPage = () => {
        this.props.history.push('/edituser')
    }

    logout = () => {
        // this.props.addUser({});
        const { push } = this.props
        push('/login')
    }

    editPage = () => {
        // this.props.addUser({});
        const { push } = this.props
        push('/editprofile')
    }




    render() {
        const { user, editUser } = this.props
        
        console.log(user)


        console.log("kuy", this.state.products)
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <View style={styles.header}>
                        <Text style={{ color: 'white' }}>PROFILE</Text>
                    </View>
                </View>
                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <ScrollView style={styles.scrollContainer}>
                    {/* {!this.state.isLogin ? */}
                        <View style={[styles.boxContent]}>
                            <TouchableOpacity style={[styles.loginButton]} onPress={() => this.navigateToLogin()}>
                                <Text style={{ fontSize: 14, color: 'black' }}>LOG IN </Text>
                            </TouchableOpacity>
                            <Text style={{ margin: 10, color: 'white' }}>OR</Text>
                            <TouchableOpacity style={[styles.regisButton]} onPress={() => this.navigateToRegis()}>
                                <Text style={{ fontSize: 14, color: '#CDCD8F' }}>REGIS </Text>
                            </TouchableOpacity>

                        </View> 
                        {/* // :null} */}
                        {/* {this.state.isLogin ? */}

                        <View style={styles.content}>
                            <TouchableOpacity onPress={this.onPressImage} style={[styles.imgProfile]}>
                                <View style={{ position: 'absolute', backgroundColor: 'white', borderRadius: 100, }}>
                                    <Icon name="camera" size="lg" color="red" />
                                </View>
                                <Image
                                    style={{ width: '100%', height: '100%', borderRadius: 100 }}
                                    source={{ uri: this.state.imagePath }}
                                >
                                </Image>

                            </TouchableOpacity>

                            <View>
                                <Text style={styles.title}>Email :</Text>
                                <Text style={styles.text}>&nbsp;&nbsp;{user.email}</Text>
                            </View>
                            <View>
                                <Text style={styles.title}>First Name :</Text>
                                <Text style={styles.text}>&nbsp;&nbsp;{user.firstName}</Text>
                            </View>
                            <View>
                                <Text style={styles.title}>Last Name :</Text>
                                <Text style={styles.text}>&nbsp;&nbsp;{user.lastName}</Text>
                            </View>
                            <Button
                                style={styles.buttons}
                                type="primary"
                                onPress={() => this.editPage()}
                            // onPress={() => this.setModalVisible(true)}
                            >
                                <Icon name="edit" size="sm" color="white" /> Edit
                    </Button>
                        </View>
                        {/* // :null
                    // } */}




                </ScrollView>
                <View style={styles.footer}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#CDCD8F"
                        barTintColor="black"
                        opacity="0.6"
                    >
                        <TabBar.Item
                            title="Movie"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === 'movieTab'}
                            onPress={() => this.navigateToMain()}
                        >
                            {/* {this.renderContent('Life Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="star" />}
                            title="Mymovie"
                            selected={this.state.selectedTab === 'myMovieTab'}
                            onPress={() => this.navigateToHistory()}
                        >
                            {/* {this.renderContent('Koubei Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="user" />}
                            title="Profile"
                            selected={this.state.selectedTab === 'profileTab'}
                            onPress={() => this.navigateToProfile()}
                        >
                            {/* {this.renderContent('Friend Tab')} */}
                        </TabBar.Item>

                    </TabBar>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        opacity: 1

    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        margin: 20,
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        flexDirection: 'row'
    },
    TextContent: {
        flexDirection: 'row',
    },
    footer: {
        height: 50,
        backgroundColor: 'black',
        flexDirection: 'row',
        opacity: 20
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    scrollContainer: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
        flexDirection: 'column',
    },
    boxContent: {
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 1
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    },
    cardTextWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 4,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardText: {
        color: '#CDCD8F',
        fontWeight: 'bold'
    },
    bannerImg: {
        height: 'auto',
    },
    footing: {
        width: 'auto'
    },
    loginButton: {
        height: 50,
        width: 350,
        marginHorizontal: 20,
        borderRadius: 5,
        backgroundColor: '#CDCD8F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    regisButton: {
        height: 50,
        width: 350,
        marginHorizontal: 20,
        borderRadius: 5,
        backgroundColor: '#39281B',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'pink'
    },
    imgProfile: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
    },




})


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(ProfilePage)

