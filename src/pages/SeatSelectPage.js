import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, Dimensions } from 'react-native';
import { TabBar, Icon, ActivityIndicator, Button, Tabs, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios';
import seatSofa from '../img/seat/seat3.png'
import seatSofa_check from '../img/seat/user.png'


var options = { year: 'numeric', month: 'long', day: 'numeric' }

class SeatSelectPage extends Component {

    state = {
        movie: [],
        seats: [],
        booking: [],
        check: true,
        total: [],
        new: [],
        loading: true


    }

    UNSAFE_componentWillMount() {
        const item = this.props.location.state.item.item
        this.renderMovie()
        this.setState({ movie: this.props.location.state.item.item })
        this.renderSeat();
        this.setState({ seats: item.seats })
  


    }

    renderMovie = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${this.props.location.state.item.item._id}`)
            .then(response => {
                this.setState({
                    movie3: response.data,
                    loading: false
                })
                this.renderSeat(response.data.seats);
                console.log('renderMovie response: ', response)
            })
            .catch((error) => {
                console.error('renderMovie error:', error);
            })
            .finally(() => { console.log('Finally') })
    }


    renderImage = () => {
        var imgSource = this.state.check ? seatSofa : seatSofa_check
        return (
            <View>
                <Image
                    source={imgSource}
                />
            </View>
        )
    }


    onClickBack = () => {
        const { push } = this.props
        const item = this.props.location.state.movie
        console.log('Back', item)
        push('/product', {
            item: item
        })
    }

    renderOrderSeat = (item) => {
        if (item.item.type === "SOFA") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat3} source={require('../img/seat/seat3.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else if (item.item.type === "PREMIUM") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('../img/seat/seat2.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('../img/seat/seat1.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
    }

    checkSeat = (item) => {
        if (item === false || this.state.seat === false) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../img/seat/seat3.png')} />
            )
        } else if (item === true) {
            return (
                <Image style={styles.seat3B} source={require('../img/seat/user.png')} />
            )
        } else if (this.state.seat === true) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../img/seat/seat3.png')} />
            )
        }
        else {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../img/seat/user.png')} />
            )
        }
    }

    onPressSeat = (type, row, column, price, items) => {

        let item = this.state.new
        item = item.map(i => {
            if (i.type !== type) {
                return i
            }
            return Object.keys(i).reduce((sum, each, indexRows) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map((item, indexCulumn) => {
                        if (each === `row${(row + 1)}` && indexCulumn === column && item === 'F') {
                            this.state.total.push(price)
                            return 'C'
                        }
                        else {
                            if (each === `row${(row + 1)}` && indexCulumn === column && item === 'C') {
                                return 'F'
                            } else {
                                return item
                            }
                        }
                    })
                    // console.log('sumEach: ', sum[each], each)
                }
                //(indexCulumn === column && indexRows === row) ? 'C': item
                return sum
            }, {})
        })
        console.log('pressSeat item: ', item);
        this.setState({ new: item })
        console.log('pressSeat items', items)
        if (items === 'F') {
            this.setState({
                booking: [...this.state.booking, {
                    type: type,
                    row: (row + 1),
                    column: (column + 1)
                }]
            })
        } else if (items === 'C') {
            let result = this.state.booking.filter((item) => {
                return !(item.row === row && item.column === column && item.type === type)
            })
            console.log('resultbookingChack:', result);
            this.setState({ booking: result })
        }
        console.log('pressSeat Booking,:', this.state.booking)
    }



    navigatePayment = () => {
        this.props.history.push('/payment', {
            id: this.props.location.state.item.item,
            movie: this.props.location.state.item,
            seats: this.state.booking,
            item: this.state.movie,
            back: this.props.location.state.movie
        })
    }


    renderSeat = () => {
        let item = this.props.location.state.item.item.seats
        item = item.map(i => {
            return Object.keys(i).reduce((sum, each) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map(item => item === true ? 'B' : 'F')
                    console.log();
                }
                return sum
            }, {})
        })
        console.log('render seat item : ', item);
        this.setState({ new: item })
    }

    Booking = () => {
        const { user } = this.props
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user.token}` },
            data: {
                showtimeId: this.state.movie._id,
                seats: this.state.booking
            }
        })
            .then(() => {
                alert('Booking is Success')
            })
            .catch((error) => {
                console.log('booking error :', error.response);
                alert(error.response)
            })
    }




    _keyExtractorSeats = (item, index) => item._id

    _keyExtractorDetailSeat = (item, index) => item._id




    render() {
        console.log("check movie", this.state.movie)
        const item = this.props.location.state.item.item
        console.log('itemM : ', item)
        const movie = this.props.location.state.item.item.movie
        var date = new Date(item.startDateTime)
        timeStart = date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        var date2 = new Date(item.endDateTime)
        timeEnd = date2.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        date = date.toLocaleDateString("th-TH", options)
        var cinema = this.props.location.state.item.item.cinema.name
        console.log('row test: ', this.state.new)
        console.log('Booking(render):', this.state.booking)

        return (


            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Text style={{ color: '#CDCD8F', fontSize: 14 }}><Icon name="left" size="md" color='#CDCD8F' />Back</Text>
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text style={{ color: 'white' }}>SEAT SELECTION</Text>
                    </View>
                    <View style={styles.back}>

                    </View>

                
                
                
                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />

                <View style={styles.HeaderContent}>
                    <View style={styles.detailMovie}>
                        <Image
                            source={{ uri: movie.image }}
                            style={{ width: '20%', height: 100, resizeMode: 'contain' }}
                        />
                        <View style={styles.infoMovie}>
                            <Text style={{ color: '#0099ff', margin: 5 }}>{date}</Text>
                            <Text style={{ color: 'white', margin: 5 }}><Icon name="clock-circle" size="xs" /> {timeStart} - {timeEnd}</Text>
                            <Text style={{ color: 'white', margin: 5 }}>{cinema} | &nbsp;<Icon name="sound" size="xxs" color="#333333" /> {item.soundtrack}</Text>
                        </View>
                    </View>
                </View>
                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <FlatList
                    keyExtractor={this._keyExtractorDetailSeat}
                    numColumns={item.seats.length}
                    data={item.seats}
                    renderItem={(item) => (
                        this.renderOrderSeat(item)
                    )}
                />
                {/* <View style={{ flexDirection: 'row' }}>
                    <Text style={{ color: 'black', fontSize: 14, justifyContent: 'flex-end' }}>Total: {this.state.total.reduce((x, y) => { return x + y }, 0)} THB</Text>
                </View> */}



                
                **
                **-{/* <View style={styles.profileBox}>

                    <View>
                        <Text style={styles.text}>Member : &nbsp;&nbsp;{[user.firstName, user.lastName]}</Text>
                    </View>
                </View> */}

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <ScrollView>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Image style={styles.screen} source={require('../img/seat/screen2.png')} />
                    </View>
                    <View style={styles.content}>

                        {
                            this.state.loading === false ? (
                                this.state.new.map((i, index) => {
                                    console.log('i: ', i)
                                    return Object.keys(i).map((rowsData, indexRows) => {
                                        
                                        console.log('rowsData: ', rowsData)
                                        
                                        return (
                                            <FlatList
                                                keyExtractor={this._keyExtractorSeats}
                                                data={i['row' + (indexRows + 1)]}
                                                numColumns={i.columns}
                                                extraData={this.state}
                                                renderItem={({ item, index }) => {
                                                    console.log('eieiei', i.type)
                                                    if (item === 'F') {
                                                        if (i.type === "SOFA") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat3B} source={require('../img/seat/seat3.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else if (i.type === "PREMIUM") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price), item }}>
                                                                    <Image style={styles.seat1B} source={require('../img/seat/seat2.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else if (i.type === "DELUXE") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat1B} source={require('../img/seat/seat1.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                    }
                                                    else if (item === 'C') {
                                                        if (i.type === "SOFA") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat3B} source={require('../img/seat/check2.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat1B} source={require('../img/seat/check.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                    }
                                                    else if (item === 'B') {
                                                        return <Image style={styles.seat1B} source={require('../img/seat/user.png')} />
                                                    }
                                                }
                                                }
                                            />
                                        )
                                    })
                                }).reverse()
                            ) : (
                                    <ActivityIndicator color="#00ccff" size="large" style={{ alignItems: 'center', justifyContent: 'center' }} />

                                )
                        }
                    </View>

                </ScrollView>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    
                    <View>

                        <TouchableOpacity style={[styles.loginButton]} onPress={() => this.navigatePayment()}>
                            <Text style={{ fontSize: 14, color: 'black' }}>Booking </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
    boxHeader: {
        backgroundColor: 'black',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 5,
        margin: 2,

        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#33353a',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 25,
        height: 25
    },
    seat3: {
        width: 40,
        height: 25
    },
    seat1B: {
        width: 8,
        height: 8,
        width: 10,
        height: 10,
        margin: 1,
        margin: 2.5,
    },
    seat3B: {
        width: 25,
        height: 8,
        margin: 2.5,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 12
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: '100%',
        height: 50,
        borderRadius: 5,
        marginBottom: 15,
    },

    footing: {
        width: 'auto'
    },
    profileBox: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        height: 40,
        margin: 20,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 15,
        flexDirection: 'row'
    },
    loginButton: {
        height: 35,
        width: 180,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: '#CDCD8F',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}


export default connect(mapStateToProps, { push })(SeatSelectPage)
