import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { Button, Card, Icon } from '@ant-design/react-native';
import axios from 'axios'
var totalprice = 0
class PaymentPage extends Component {
    state = {
        movie: [],
        seats: [],
        total: 0
    }
    UNSAFE_componentWillMount() {
        this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.seats })
    }
    goToMenu = () => {
        const { user } = this.props
        // console.log('ID', this.state.movie._id)
        console.log('user', user.token)
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user.token}` },
            data: {
                showtimeId: this.props.location.state.id._id,
                seats: this.state.seats
            }
        })
            .then(() => {
                alert('Booking is Success')
                const { push } = this.props
                push('/main', {
                    item: this.props.location.state.movie, movie: this.props.location.state.item
                })
                totalprice = 0
            })
            .catch((error) => {
                console.log(error);
                alert(error,"plz login to booking")
            })
    }

    price = (item) => {
        if (item === 'SOFA') {
            totalprice = totalprice + 500
            this.setState({ total: this.state.total + 500 })
            return 500
        } else if (item === 'PREMIUM') {
            totalprice = totalprice + 190
            this.setState({ total: this.state.total + 190 })
            return 190
        } else {
            totalprice = totalprice + 150
            this.setState({ total: this.state.total + 150 })
            return 150
        }
    }

    onClickBack = () => {
        const { push } = this.props
        const item = this.props.location.state.movie
        push('/seat', {
            item: this.props.location.state.movie, movie: this.props.location.state.back
        })
    }

    render() {
        const { user } = this.props
        console.log('render check movie:', this.state.movie);
        console.log('render check seat:', this.state.seats);
        console.log('render check token:', user.token);
        console.log('render check img :', this.state.movie.item.movie.image);

        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Text style={{ color: '#CDCD8F', fontSize: 14 }}><Icon name="left" size="md" color='#CDCD8F' />Back</Text>
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text style={{ color: 'white' }}>PAYMENT</Text>
                    </View>
                    <View style={styles.back}>

                    </View>

                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <FlatList
                        data={this.state.seats}
                        renderItem={({ item }) =>
                            <Card style={{ width: 300 }}>
                                <Card.Header
                                    title='Ticket'
                                />
                                <Card.Body>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image
                                            source={{ uri: this.state.movie.item.movie.image }}
                                            style={{ width: 70, height: 70, resizeMode: 'contain' }}
                                        />
                                        <View>
                                            <Text style={{ fontSize: 13 }}> SeatType:{item.type} </Text>
                                            <Text style={{ fontSize: 15 }}> R:{item.row} C:{item.column} </Text>
                                            <Text style={{ fontSize: 15 }}> Price: {this.price(item.type)} THB </Text>
                                        </View>
                                        <View style={{ alignItems: 'center' }}>

                                        </View>
                                    </View>
                                </Card.Body>
                            </Card>
                        } />
                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />

                <View style={{ backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18, color: '#CDCD8F' }}>Total : {totalprice} THB</Text>

                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />
                <View>
                <TouchableOpacity style={[styles.loginButton]} onPress={() => this.goToMenu()}>
                    <Text style={{ fontSize: 14, color: 'black' }}>Confirm </Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
    boxHeader: {
        backgroundColor: 'black',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#34444b',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 15,
        height: 15
    },
    seat3: {
        width: 40,
        height: 15
    },
    seat1B: {
        width: 10,
        height: 10,
        margin: 1
    },
    seat3B: {
        width: 25,
        height: 8,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 8
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: '100%',
        height: 50,
        borderRadius: 20,
    },
    footing: {
        width: 'auto'
    },
    loginButton: {
        height: 35,
        width: 180,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: '#CDCD8F',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

});


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
export default connect(mapStateToProps, { push })(PaymentPage)



