import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Modal,
    Image,
    TextInput,
    TouchableOpacity, Alert, FlatList, Dimensions
} from 'react-native'
import { Button, Icon, InputItem, WhiteSpace, TabBar, Card } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'

import axios from 'axios'

class HistoryPage extends React.Component {

    state = {
        tickets: [],
        ticket: []
    }


    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'redTab',
        };
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    onClickBack = () => {
        this.props.history.push('/main', {
            item: 'profile'
        })
    }

    UNSAFE_componentWillMount() {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: { 'Authorization': `bearer ${user.token}` }
        })
            .then(res => {
                console.log('RES:', res.data);
                const dataTickets = res.data
                this.setState({ tickets: dataTickets })
                for (let index = 0; index < dataTickets.length; index++) {
                    console.log('Ticket INdex?:', dataTickets[index]);
                    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${dataTickets[index].showtime}`)
                        .then(res => {
                            this.setState({ ticket: [...this.state.ticket, res.data] })
                            console.log('ticket:', this.state.ticket);
                            console.log('ticketS:', this.state.tickets);
                        })
                        .catch(error => {
                            console.log('ERROR_showtime:', error);
                        })
                }
            })
            .catch(error => {
                console.log('ERROR:', error);
            })
    }

    seatPrint = (seats) => {
        const seat = []
        for (let index = 0; index < seats.length; index++) {
            seat.push(<Text>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        return seat
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }

    _keyExtractor = (item, index) => item._id


    navigateToProfile = () => {
        this.props.history.push('/profile')
    }

    navigateToMain = () => {
        this.props.history.push('/main')
    }

    render() {
        const { user, editUser } = this.props
        console.log(user)
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <View style={styles.header}>
                        <Text style={{ color: 'white' }}>MY MOVIE</Text>
                    </View>
                </View>

                <Image style={styles.footing}
                    source={require('../img/object/footing.png')}
                />

                <ScrollView style={styles.scrollContainer}>
                    <View style={{ margin: 5 }}>
                        <FlatList
                            keyExtractor={this._keyExtractor}
                            inverted data={this.state.ticket}
                            renderItem={({ item, index }) =>
                                <Card>
                                    <Card.Header
                                        title={item.movie.name}
                                        extra={item.movie.duration + ' min'}
                                    />
                                    <Card.Body>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ margin: 5 }}>
                                                <TouchableOpacity>
                                                    <Image style={{ width: 100, height: 180, resizeMode: 'contain' }}
                                                        source={{ uri: item.movie.image }} />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ margin: 5 }}>
                                                <Text>{new Date(this.state.tickets[index].createdDateTime).toLocaleDateString()}</Text>
                                                {item.index}{this.seatPrint(this.state.tickets[index].seats)}
                                                <Text> Show time : {this.addZero(new Date(item.startDateTime).toLocaleDateString())} </Text>
                                                <Text> {this.addZero(new Date(item.startDateTime).getHours())} : {this.addZero(new Date(item.startDateTime).getMinutes())}</Text>
                                            </View>
                                        </View>
                                    </Card.Body>
                                    <Card.Footer
                                        content="Soundtrack / Subtitle"
                                        extra={item.movie.soundtracks + '/' + item.movie.subtitles}
                                    />
                                </Card>
                            }
                        />
                    </View>
                 
                </ScrollView>
                <View style={styles.footer}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#CDCD8F"
                        barTintColor="black"
                        opacity="0.6"
                    >
                        <TabBar.Item
                            title="Movie"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === 'blueTab'}
                            onPress={() => this.navigateToMain()}
                        >
                            {/* {this.renderContent('Life Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="star" />}
                            title="Mymovie"
                            selected={this.state.selectedTab === 'redTab'}
                            onPress={() => this.onChangeTab('redTab')}
                        >
                            {/* {this.renderContent('Koubei Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="user" />}
                            title="Profile"
                            selected={this.state.selectedTab === 'greenTab'}
                            onPress={() => this.navigateToProfile()}
                        >
                            {/* {this.renderContent('Friend Tab')} */}
                        </TabBar.Item>

                    </TabBar>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',

    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 50,
        backgroundColor: 'black',
        flexDirection: 'row',
        opacity: 20
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    scrollContainer: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
        flexDirection: 'column',
    },
    boxContent: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    },
    slideContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 120,
    },
    wrapper: {
        backgroundColor: '#fff',
    },
    cardWrapper: {
        flex: 1,
        padding: 5,
        backgroundColor: 'black',
    },
    cardImage: {
        width: 186.7,
        height: 276.3,
    },
    cardTextWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 4,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardText: {
        color: '#CDCD8F',
        fontWeight: 'bold'
    },
    bannerImg: {
        height: 'auto',
    },
    footing: {
        width: 'auto'
    },



})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(HistoryPage)



