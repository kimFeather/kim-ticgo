import React from 'react';
import { StyleSheet,
     Text, 
     View, 
     ScrollView, 
     Button, 
     Image, 
     TouchableOpacity, Alert, FlatList, Dimensions } from 'react-native'
import { Carousel, Icon, SearchBar, TabBar, Card } from '@ant-design/react-native';
import axios from 'axios'

class TicketPage extends React.Component {

    state = {
        products: []
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'movieTab',
        };
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    UNSAFE_componentWillMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => {
                return response.data
            })
            .then(data => {
                this.setState({ products: data })
                console.log(data)
            })
    }

    clickProduct = (product) => {
        console.log(product);         
        const tempState = this.state
        tempState.productClick = product
        this.props.history.push('/product' , {item:product})
        
    }

    navigateToProfile = () => {
        this.props.history.push('/profile')
    }

    navigateToHistory = () => {
        this.props.history.push('/history')
    }

    render() {

        console.log("kuy", this.state.products)
        return (
            <View style={styles.container}>
                <View style={styles.headerSection}>
                    <View style={styles.header}>
                        {/* <Text style={{ fontSize: 24, color: 'white' }}>Main Page</Text> */}
                        <Image style={{height:40 , width:40}}
                    source={require('../img/logo/muraKim.png')}
                />
                    </View>
                   
                </View>
                <Image style={styles.footing}
                       source={require('../img/object/footing.png')}
                            />
                <ScrollView style={styles.scrollContainer}>
                    

                </ScrollView>
                <View style={styles.footer}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#CDCD8F"
                        barTintColor="black"
                        opacity="0.6"
                    >
                        <TabBar.Item
                            title="Movie"
                            icon={<Icon name="home" />}
                            selected={this.state.selectedTab === 'movieTab'}
                            onPress={() => this.onChangeTab('blueTab')}
                        >
                            {/* {this.renderContent('Life Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="star" />}
                            title="Mymovie"
                            selected={this.state.selectedTab === 'myMovieTab'}
                            onPress={() => this.navigateToHistory()}
                        >
                            {/* {this.renderContent('Koubei Tab')} */}
                        </TabBar.Item>
                        <TabBar.Item
                            icon={<Icon name="user" />}
                            title="Profile"
                            selected={this.state.selectedTab === 'profileTab'}
                            onPress={() => this.navigateToProfile()}
                        >
                            {/* {this.renderContent('Friend Tab')} */}
                        </TabBar.Item>

                    </TabBar>
                  
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        
    },
    headerSection: {
        height: 40,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    content: {
        backgroundColor: '#f7f7f7',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    footer: {
        height: 50,
        backgroundColor: 'black',
        flexDirection: 'row',
        opacity: 20
    },
    squareButton: {
        height: 60,
        width: 60,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    scrollContainer: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column'
    },
    scrollContent: {
        flex: 1,
        flexDirection: 'row',
    },
    contentChild: {
        flex: 1,
        flexDirection: 'column',
    },
    box: {
        height: 180,
        backgroundColor: '#ffffff',
        margin: 10
    },
    boxText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 16
    },
    slideContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: 120,
    },
    wrapper: {
        backgroundColor: '#fff',
    },
    cardWrapper: {
        flex: 1,
        padding: 5,
        backgroundColor: 'black',
    },
    cardImage: {
        width: 186.7,
        height: 276.3,
    },
    cardTextWrapper: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        paddingVertical: 4,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardText: {
        color: '#CDCD8F',
        fontWeight: 'bold'
    },
    bannerImg:{
        height: 'auto',
    },
    footing:{
        width: 'auto'
    }
    


})

export default TicketPage